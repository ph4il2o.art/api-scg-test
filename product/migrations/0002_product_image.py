# Generated by Django 3.2 on 2021-04-23 10:38

from django.db import migrations
import product.models
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='image',
            field=sorl.thumbnail.fields.ImageField(blank=True, null=True, upload_to=product.models.image_product_path),
        ),
    ]
