import logging

from rest_framework import serializers

from product.models import Product

logger = logging.getLogger(__name__)


class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = '__all__'
