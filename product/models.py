from datetime import datetime
from django.db import models
import time as times
import os
from sorl.thumbnail import ImageField


def image_product_path(instance, filename):
    now = str(int(times.time()))
    file_path = os.path.join('product', now)

    ext = filename.split('.')[-1]
    filename = '{}.{}'.format(now, ext)
    return os.path.join(file_path, filename)


class Product(models.Model):
    name = models.CharField(max_length=30)
    image = ImageField(upload_to=image_product_path, blank=True, null=True)
    created_at = models.DateTimeField(default=datetime.now, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)

