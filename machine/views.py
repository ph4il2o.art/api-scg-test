from rest_framework import viewsets
from rest_framework.views import APIView

from machine.models import Stock, VendingMachine
from machine.serializers import StockSerializer, VendingMachineSerializer, VendingReversSerializer


class VendingMachineViewSet(viewsets.ModelViewSet):
    queryset = VendingMachine.objects.all()
    serializer_class = VendingMachineSerializer


class StockViewSet(viewsets.ModelViewSet):
    queryset = Stock.objects.all()
    serializer_class = StockSerializer


class MachineList(viewsets.ModelViewSet):
    queryset = VendingMachine.objects.all()
    serializer_class = VendingReversSerializer
