from django.db import models
from datetime import datetime

from product.models import Product


class VendingMachine(models.Model):
    name = models.CharField(max_length=30)
    lat = models.DecimalField(max_digits=22, decimal_places=16, blank=True, null=True)
    lng = models.DecimalField(max_digits=22, decimal_places=16, blank=True, null=True)
    created_at = models.DateTimeField(default=datetime.now, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)


class Stock(models.Model):
    amount = models.IntegerField(default=0)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    machine = models.ForeignKey(VendingMachine, on_delete=models.CASCADE)
    out_stock = models.BooleanField(default=False)
    created_at = models.DateTimeField(default=datetime.now, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
