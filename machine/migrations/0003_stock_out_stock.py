# Generated by Django 3.2 on 2021-04-26 10:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('machine', '0002_auto_20210423_1102'),
    ]

    operations = [
        migrations.AddField(
            model_name='stock',
            name='out_stock',
            field=models.BooleanField(default=False),
        ),
    ]
