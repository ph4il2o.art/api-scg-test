from rest_framework import serializers

from machine.models import VendingMachine, Stock
from product.models import Product
from product.serializers import ProductSerializer
import logging

logger = logging.getLogger(__name__)

class VendingMachineSerializer(serializers.ModelSerializer):
    class Meta:
        model = VendingMachine
        fields = '__all__'


class StockSerializer(serializers.ModelSerializer):

    class Meta:
        model = Stock
        fields = '__all__'


class StockWithProductSerializer(serializers.ModelSerializer):
    product = ProductSerializer(read_only=True)

    class Meta:
        model = Stock
        fields = ['id', 'product', 'amount']


class VendingReversSerializer(serializers.ModelSerializer):
    stock = serializers.SerializerMethodField()

    class Meta:
        model = VendingMachine
        fields = '__all__'

    def get_stock(self, obj):
        try:
            stock = Stock.objects.filter(machine__pk=obj.pk)
            serializer = StockWithProductSerializer(stock, many=True)
            return serializer.data
        except Stock.DoesNotExist:
            return None
